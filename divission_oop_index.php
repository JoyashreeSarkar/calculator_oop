<?php
function __autoload($classname)
{
    include_once $classname . ".php";
}


//creating a new object($addition1) from class (Addition)
$divission1 = new Divission();
$result = "The divission of two number is: " . $divission1->divide2numbers($_POST['number1'], $_POST['number2']);

$displayer = new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre($result);
$displayer->displayitalic($result);
$displayer->displayh1($result);