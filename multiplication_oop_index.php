<?php
function __autoload($classname)
{
    include_once $classname . ".php";
}

//creating a new object($addition1) from class (Addition)
$multiplication1=new Multiplication();
$result="The multiplication of two number is: ".$multiplication1->multiple2numbers($_POST['number1'],$_POST['number2']);

$displayer= new Displayer();
$displayer->displaysimple($result);
$displayer->displaypre ($result);
$displayer->displayitalic ($result);
$displayer->displayh1 ($result);